<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'homefunc']);
Route::get('/register', [AuthController::class, 'registerfunc']);
Route::post('/sendata', [AuthController::class, 'konfirmdata']);


Route::get('/data-table', function () {
  return view('halaman.data-table');
});

Route::get('/table', function () {
  return view('halaman.table');
});

//Kategori
// Form
Route::get('/kategori/create/', [KategoriController::class, 'create']);
// Send Data
Route::post('/kategori', [KategoriController::class, 'store']);

// Read Data
Route::get('/kategori', [KategoriController::class, 'index']);

// Read Data Detail
Route::get('/kategori/{kategori_id}', [KategoriController::class, 'show']);

// Update Data
// Form Update
Route::get('kategori/{kategori_id}/edit', [KategoriController::class, 'edit']);

//Update ke DB
Route::put('/kategori/{kategori_id}', [KategoriController::class, 'update']);

// Delete Data
Route::delete('/kategori/{kategori_id}', [KategoriController::class, 'destroy']);



Route::get('/cast/create/', [CastController::class, 'create']);
// Send Data
Route::post('/cast', [CastController::class, 'store']);

// Read Data
Route::get('/cast', [CastController::class, 'index']);

// Read Data Detail
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update Data
// Form Update
Route::get('cast/{cast_id}/editcast', [CastController::class, 'edit']);

//Update ke DB
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
