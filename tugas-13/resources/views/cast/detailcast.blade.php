@extends('layout.master')

@section('judul')
    Detail Cast
@endsection


@section('content')
    <h1>{{ $cast->nama }} (Umur {{ $cast->umur }})</h1>
    <p>{{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-sm btn-secondary">Kembali</a>
@endsection
