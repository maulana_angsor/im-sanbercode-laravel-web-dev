@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection


@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm mb-2">Tambah</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Biodata</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=> $val)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ $val->umur }}</td>
                    <td>{{ $val->bio }}</td>
                    <td>
                        <form action="/cast/{{ $val->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <a href="/cast/{{ $val->id }}" class="btn btn-info">Detail</a>
                            <a href="/cast/{{ $val->id }}/editcast" class="btn btn-warning">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
