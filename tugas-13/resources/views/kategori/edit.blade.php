@extends('layout.master')

@section('judul')
    Halaman Edit Kategori
@endsection


@section('content')
    <form action="/kategori/{{ $kategori->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" name="nama" class="form-control" value="{{ $kategori->nama }}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi Kategori</label>
            <textarea name="deskripsi" class="form-control" cols="10" rows="10">{{ $kategori->deskripsi }}</textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
