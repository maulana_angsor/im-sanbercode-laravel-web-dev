@extends('layout.master')

@section('judul')
    Halaman List Kategori
@endsection


@section('content')
    <a href="/kategori/create" class="btn btn-primary btn-sm mb-2">Tambah</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key=> $val)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ $val->deskripsi }}</td>
                    <td>
                        <form action="/kategori/{{ $val->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <a href="/kategori/{{ $val->id }}" class="btn btn-info">Detail</a>
                            <a href="/kategori/{{ $val->id }}/edit" class="btn btn-warning">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
