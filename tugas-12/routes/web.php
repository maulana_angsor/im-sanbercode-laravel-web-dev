<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'homefunc']);
Route::get('/register', [AuthController::class, 'registerfunc']);
Route::post('/sendata', [AuthController::class, 'konfirmdata']);


Route::get('/data-table',function(){
return view('halaman.data-table');
});

Route::get('/table',function(){
  return view('halaman.table');
  });