@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection


@section('content')
  <form action="/sendata" method="post">
    @csrf
    <h3>Sign Up Form</h3>
    <p>First name:</p>
    <input type="text" name="fname" />
    <p>Last name:</p>
    <input type="text" name="lname" />
    <p>Gender:</p>
    <input type="Radio" name="kelamin" />Male<br />
    <input type="Radio" name="kelamin" />Female<br />
    <input type="Radio" name="kelamin" />Other<br />
    <p>Nationality:</p>
    <select name="negara">
      <option>Indonesian</option>
      <option>Malaysia</option>
      <option>Singapore</option>
    </select>
    <p>Language Spoken:</p>
    <input type="checkbox" name="bahasa" />Bahasa Indonesia<br />
    <input type="checkbox" name="bahasa" />English<br />
    <input type="checkbox" name="bahasa" />Other<br />
    <p>Bio:</p>
    <textarea cols="30" rows="10" name="biodata"></textarea><br /><br />
    <button type="submit">Sign Up</button>
  </form>
  @endsection