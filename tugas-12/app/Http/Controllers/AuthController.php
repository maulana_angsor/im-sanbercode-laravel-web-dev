<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerfunc()
    {
        return view('register');
    }
    public function konfirmdata(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('welcome', ['firstn' => $fname, 'lastn' => $lname]);
    }
}
