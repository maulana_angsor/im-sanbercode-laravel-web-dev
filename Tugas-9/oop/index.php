<?php
require 'Ape.php';
require 'Frog.php';
$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo 'Name: '.$sheep->name; // "shaun"
echo '<br>';
echo 'Legs: '.$sheep->legs; // 4
echo '<br>';
echo 'Cold Blooded: '.$sheep->cold_blooded; // "no"
echo '<br>';
echo '<br>';

echo 'Name: '.$kodok->name; // "shaun"
echo '<br>';
echo 'Legs: '.$kodok->legs; // 4
echo '<br>';
echo 'Cold Blooded: '.$kodok->cold_blooded; // "no"
echo '<br>';
echo '<br>';

echo 'Name: '.$sungokong->name; // "shaun"
echo '<br>';
echo 'Legs: '.$sungokong->legs; // 4
echo '<br>';
echo 'Cold Blooded: '.$sungokong->cold_blooded; // "no"
echo '<br>';
echo '<br>';
